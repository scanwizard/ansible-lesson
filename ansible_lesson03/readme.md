```
sudo setenforce 0
/etc/sysconfig/selinux
SELINUX=disabled
```

```
# посмотреть булевые значения
getsebool -a
getsebool -a | grep ftp
getsebool -a | grep anon
getsebool -a | grep ftp
# выкл и вкл selinux
sestatus
setenforce 0
sestatus
setenforce 1
```

