```
https://sprosi.pro/questions/60412/yum-zavisaet-i-ne-budet-otvechat

rm -f /var/lib/rpm/__*
rpm --rebuilddb -v -v   
yum clean all
```

```
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable --now docker
sudo usermod -aG docker $(whoami)
newgrp docker
sudo ln -sf /usr/bin/python3 /usr/bin/python
sudo pip3 install docker-compose
```

```
https://github.com/ansible/awx/blob/devel/tools/docker-compose/README.md

vim tools/docker-compose/inventory

Run AWX - Start the containers
make docker-compose COMPOSE_UP_OPTS=-d

Clean and build the UI
docker exec tools_awx_1 make clean-ui ui-devel

Create an admin user
docker exec -ti tools_awx_1 awx-manage createsuperuser
Admin/

Login
https://192.168.1.200:8043/#/login

```